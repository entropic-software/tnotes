# Introduction

tnotes is a bash based note-taking application and organizer.

It will act as a regular shell, taking commands and giving feedback.
But some commands are has been expanded with functionality for ease of
organizing notes, and if you have set up git, they will also call git
transparently so your notes will be version controlled.


# Requirements

## Hard requirements

- A Unix-like shell environment

- bash

## Really good to have

- tree

    In Debian and FreeBSD, the package and command is called *tree*.

    In OpenBSD the package and command is called *colortree*.
    tnotes will try to use *colortree* first, but if it is not installed,
    and only the old OpenBSD *tree* which uses different arguments is found,
    tnotes will give it the wrong arguments and fail.
    So if you use OpenBSD, please install *colortree*.

- git

    Will be used to version control your notes, if you have prepared
    your notes directory for git. See later for instructions.

- ImageMagick

    Will be used to create an empty image when you create a new image-note.
    If not installed, the image will not be created, and the editor cannot
    open the image, thus not starting. This workflow might be improved in the 
    future.

## May be nice to have

- trash-cli

    If you want notes to not be removed directly, but put in the system's
    trashcan you should install trash-cli. It will be used if found. It seems
    to be only available in Gnu/Linux distributions.
    If you use git, trash-cli might be overkill.

---

# Configuration

In the configuration file $HOME/.tnotes.conf you may override some default
settings. The most important ones are the directory where all your notes
are saved, and the applications you use to edit and view notes.

These are the default settings:

    notes_dir="$HOME/notes"
    EDITOR=${EDITOR:-micro}
    PAGER=${PAGER:-less}
    IMAGE_EDITOR=rgbpaint
    IMAGE_VIEWER=display
    PDF_VIEWER=okular
    MARKDOWN_VIEWER="pandoc -t plain"

${EDITOR:-micro} means that if you have already defined the variable
$EDITOR in your shell environment, it will be used, otherwise *micro*
will be used. This will let you use the commonly used PAGER and EDITOR
variables from your regular bash environment.

You may also override other settings like the prompt and history file.
Look at the tnotes file if you want to dig deeper into this.

---

# Running and initial setup

Note: If you already have your notes directory (default $HOME/notes) under
git control, you should know that tnotes will execute some git commands in
the background as part of other commands. So be prepared for this!

Put tnotes somewhere in your path, and run it. If your notes directory
does not exist, tnotes will ask if it may create it on first launch.

At startup, all your notes will be displayed like a tree structure.

The prompt will show some important commands. Type *?* or *help* for
a list of all commands and how to use them. There will also be some
documentation which is not present in this document, so read it too.


## git setup

If the notes directory is not already under git control, run "git init"
after you have started tnotes. If you already have notes before running
"git init" you must add these manually before you do anything else:

    git add .
    git commit -a -m "added"

After this, tnotes will automatically add, delete, move/rename and commit
changes when you use the corresponding tnotes command.

Note that since tnotes is basically bash, you can always use regular git
commands inside tnotes. So you can examine your files with "git status"
and so on.

If you want to push/pull to a remote git repository, you may set the
remote origin as you would on an ordinary git repository, ex:

    git remote add origin ssh://user@my-git-server.org:/home/user/notes.git
    git pull origin master
    # To set default merging branch you might also want to:
    git push -u origin HEAD

After this tnotes will push after commit and pull before any command.


### Notes and quirks

There are some quirks related to empty directories and git, see BUGS for
details.

If you change anything in the notes directory with an application other
than tnotes, for example moving around files with a file manager or in
a regular shell, there will be inconsistencies. Since tnotes hides most
git related output, you might not notice this, so run "git status" if you
think this has happened, and fix the inconsistencies with ordinary git
commands.


---

# Bugs

## Remote git problems

If the network is down or there are other problems reaching your git server,
tnotes may freeze everytime you issue a command while it's waiting for reply.
No workaround exists for this, except temporary renaming your $notes_dir/.git
directory until the server begins to work again.

Sometimes when the server cannot be reached, git will not freeze, but just
respond with an error. In these cases, tnotes will display a warning message,
and continue doing things locally. The next time a push/pull is run, it will
try again until it succeeds.


## git and empty directories

Trying to rename an empty directory in a git-enabled notes directory will
fail entirely; the directory will not be renamed at all. A workaround is to
use the native rename command: "\mv oldname newname" or
"/bin/mv oldname newname"

Removing empty directories will trigger an error message from git. The
directory will be removed anyway.


## Old version of "tree" on OpenBSD

The old version of *tree* takes other arguments, and will cause the tnotes
command *list* to fail.


---

# Author

Created by Tomas Åkesson <tomas@entropic.se>

See LICENCE for license details.

Project homepage: https://gitlab.com/entropic-software/tnotes
